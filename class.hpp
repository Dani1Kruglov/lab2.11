#include <string>

#ifndef LAB2_11_CLASS_HPP
#define LAB2_11_CLASS_HPP

#endif


#pragma once


namespace cl {
    enum Lang {JAVA = 0, C_PLUS_PLUS = 1, PHP = 2, JAVA_SCRIPT = 3, PYTHON = 4 };
    class CodeGenerator {
    public:
        CodeGenerator() {}
        virtual ~CodeGenerator() {}
        std::string generateCode() {
            return someCodeRelatedThing();
        }
    protected:
        virtual std::string someCodeRelatedThing() = 0;
    };


    class JavaCodeGenerator : public CodeGenerator {
    public:
        JavaCodeGenerator() : CodeGenerator() {}
        virtual ~JavaCodeGenerator() {}

    protected:
        std::string someCodeRelatedThing() override {
            return "Some code on Java";
        }
    };

    class CppCodeGenerator : public CodeGenerator {
    public:
        CppCodeGenerator() : CodeGenerator() {}
        virtual ~CppCodeGenerator() {}

    protected:
        std::string someCodeRelatedThing() override {
            return "Some code on C++";
        }
    };


    class PHPCodeGenerator : public CodeGenerator {
    public:
        PHPCodeGenerator() : CodeGenerator() {}
        virtual ~PHPCodeGenerator() {}

    protected:
        std::string someCodeRelatedThing() override {
            return "Some code on PHP";
        }
    };


    class JavaScriptCodeGenerator : public CodeGenerator {
    public:
        JavaScriptCodeGenerator() : CodeGenerator() {}
        virtual ~JavaScriptCodeGenerator() {}

    protected:
        std::string someCodeRelatedThing() override {
            return "Some code on JAVA_SCRIPT";
        }
    };

    class PythonCodeGenerator : public CodeGenerator {
    public:
        PythonCodeGenerator() : CodeGenerator() {}
        virtual ~PythonCodeGenerator() {}

    protected:
        std::string someCodeRelatedThing() override {
            return "Some code on Python";
        }
    };

    CodeGenerator* codeFactory(enum Lang _language) {
        switch (_language) {
            case JAVA: return new JavaCodeGenerator();
            case C_PLUS_PLUS: return new CppCodeGenerator();
            case PHP: return new PHPCodeGenerator();
            case JAVA_SCRIPT: return new JavaScriptCodeGenerator();
            case PYTHON: return new PythonCodeGenerator();
        }
        throw std::logic_error("Bad language");
    }
}
