#include <iostream>

#include "class.hpp"


using namespace cl;


int main() {

    CodeGenerator* somecode = codeFactory(cl::JAVA);
    std::cout << somecode->generateCode() << std::endl;

    delete somecode;
    return 0;

}

